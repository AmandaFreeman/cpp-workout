#ifndef MANAGEDBDIALOG_H
#define MANAGEDBDIALOG_H

#include <QDialog>
#include <QAbstractButton>

namespace Ui {
class ManageDbDialog;
}

class ManageDbDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ManageDbDialog(QWidget *parent = nullptr);
    ~ManageDbDialog();

private slots:
    void on_AddExercise_clicked();

    void on_manageDbButtonBox_clicked(QAbstractButton *button);

private:
    Ui::ManageDbDialog *ui;
};

#endif // MANAGEDBDIALOG_H
