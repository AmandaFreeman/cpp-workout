#include "managedbdialog.h"
#include "ui_managedbdialog.h"
#include <QDebug>
#include <QStringList>
#include "mainwindow.h"
#include <QDialogButtonBox>

ManageDbDialog::ManageDbDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ManageDbDialog)
{
    ui->setupUi(this);
//    QStringList categories = MainWindow::get_categories_from_db();
//    ui->categoryComboBox->addItems()
}

ManageDbDialog::~ManageDbDialog()
{
    delete ui;
}

void ManageDbDialog::on_AddExercise_clicked()
{
    QString exercise = ui->exerciseLineEdit->text();
    qDebug() << exercise;
    ui->exerciseLineEdit->clear();
//    check db for matching exercises
//    list found matches
//    confirm addition
}

void ManageDbDialog::on_manageDbButtonBox_clicked(QAbstractButton *button)
{
    QDialogButtonBox::StandardButton stdButton = ui->manageDbButtonBox->standardButton(button);

    if(stdButton == QDialogButtonBox::Ok)
    {
//        send data to database
        accept();
    }
    if(stdButton == QDialogButtonBox::Cancel)
    {
        //        send data to database
        reject();
    }
}
