#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "workoutdatabase.h"
#include <vector>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QStringList get_categories_from_db();
private slots:

    void on_genButton_clicked();

    void on_manageExercises_clicked();

private:
    Ui::MainWindow *ui;
    WorkoutDatabase *database;
    std::vector<QString> exercises_vector;
    void shuffle_exercises();
    void display_new_routine();
    void clear_checkboxes();

};
#endif // MAINWINDOW_H
