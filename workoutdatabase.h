#ifndef WORKOUTDATABASE_H
#define WORKOUTDATABASE_H

#include <QFile>
#include <QTextStream>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <vector>
#include "managedbdialog.h"

// You HAVE to initialise the db like this because driver won't load if you add later: QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
class WorkoutDatabase
{
private:
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    QSqlQuery q;

public:
    WorkoutDatabase();
    ~WorkoutDatabase();

    void setup();
    void loadData();
    std::vector<QString> getExercises(int numOfRows, std::vector<QString> exercises);
    int get_number_of_exercises();
    QStringList get_categories();
};

#endif // WORKOUTDATABASE_H
