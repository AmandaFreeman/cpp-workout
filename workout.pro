QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

# add CONFIG+=sdk_no_version_check due to xcode version too new for qt
CONFIG += \
    c++17 \
    sdk_no_version_check

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    managedbdialog.cpp \
    workoutdatabase.cpp

HEADERS += \
    mainwindow.h \
    managedbdialog.h \
    workoutdatabase.h

FORMS += \
    mainwindow.ui \
    managedbdialog.ui

mac {
  Resources.files = data
  Resources.path = Contents/MacOS
  QMAKE_BUNDLE_DATA += Resources
}

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    notes.txt
