#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <string>
#include "workoutdatabase.h"
#include <QDebug>
#include <QList>
#include "managedbdialog.h"




MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , database(new WorkoutDatabase())
{
    ui->setupUi(this);
    database->setup();
    database->loadData();
    int rowCount = database->get_number_of_exercises();
    exercises_vector = database->getExercises(rowCount, exercises_vector);
    display_new_routine();
    //    for (QString wo : exercises_vector)
//    {
//        qDebug() << wo;
//    }
}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::on_genButton_clicked()
{
    display_new_routine();
}

void MainWindow::shuffle_exercises()
{
    int size = exercises_vector.size();
    long int t = static_cast<long int> (time(NULL));           // seconds since epoch (or something similar)
    int number_of_shuffles = t % 1000;                         // get the last 3 digits of t
    QString temp_exercise;
    int randomIndex;

    for (int shuffle = 0; shuffle < number_of_shuffles; shuffle++){
        // for each exercise in the vector, swap it with a random other exercise
        for (int i=0;i<size;i++){
            randomIndex = rand() % size;                       // get a random number between 0 and size
            temp_exercise = exercises_vector[i];
            exercises_vector[i] = exercises_vector[randomIndex];
            exercises_vector[randomIndex] = temp_exercise;
        }
    }
}

void MainWindow::display_new_routine()
{
    shuffle_exercises();
    clear_checkboxes();
    for (int i = 0; i < ui->v_l->count(); ++i)
    {
      QWidget *cb = ui->v_l->itemAt(i)->widget();
      if (cb != NULL)
      {
        QCheckBox *ccc = static_cast<QCheckBox*>(cb);
        ccc->setText(exercises_vector[i]);
      }
      else
      {
        // not sure yet...
      }
    }
}

void MainWindow::clear_checkboxes()
{
    for (int i = 0; i < ui->v_l->count(); ++i)
    {
      QWidget *cb = ui->v_l->itemAt(i)->widget();
      if (cb != NULL)
      {
        QCheckBox *ccc = static_cast<QCheckBox*>(cb);
        ccc->setChecked(false);
      }
      else
      {
        // not sure yet...
      }
    }
}

void MainWindow::on_manageExercises_clicked()
{
    ManageDbDialog * dbDialog = new ManageDbDialog(this);
//    dbDialog->exec(); // best to avoid useing exec() apparently
    dbDialog->open();
    connect(dbDialog, &ManageDbDialog::accepted, [=](){
//        update database

    });
    connect(dbDialog, &ManageDbDialog::rejected, [=](){

    });
}

QStringList MainWindow::get_categories_from_db()
{
    QStringList *categories = new QStringList();
    *categories = database->get_categories();
    return *categories;
}
