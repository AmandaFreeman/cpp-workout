#include "workoutdatabase.h"

#include <QSqlError>
#include <QDebug>
#include <QFile>
#include <QTextStream>
#include <QCoreApplication>
#include <vector>
#include <QStringList>

WorkoutDatabase::WorkoutDatabase()
{
    db.setDatabaseName(QCoreApplication::applicationDirPath().append("/data/workout.db"));
}

WorkoutDatabase::~WorkoutDatabase()
{
    db.close();
}

void WorkoutDatabase::setup()
{
    try
    {
        if (!db.open())
        {
            qDebug() << "Database not open!";
        }
        else
        {
            if (!q.exec("CREATE TABLE IF NOT EXISTS exercises (          \
                       id INTEGER PRIMARY KEY,                           \
                       name VARCHAR(25) NOT NULL UNIQUE,                 \
                       description VARCHAR(255),                         \
                       category VARCHAR(10) NOT NULL,                    \
                       count MEDIUMINT NOT NULL DEFAULT 0,               \
                       total_time TIME DEFAULT 0,                        \
                       enabled BOOLEAN DEFAULT 1 NOT NULL)"))
            {
                    qDebug() << "Problem executing 'CREATE TABLE IF NOT EXISTS exercises'";
            }

            q.clear();

            if (!q.exec("CREATE TABLE IF NOT EXISTS routines (           \
                       completed TIMESTAMP NOT NULL PRIMARY KEY,         \
                       json_data JSON NOT NULL,                          \
                       start_time TIMESTAMP NOT NULL,                    \
                       end_time TIMESTAMP NOT NULL                       \
                       )"))
            {
                qDebug() << "Problem executing 'CREATE TABLE IF NOT EXISTS routines'";
            }

            q.clear();

            qDebug() << "TABLES IN DB: " << db.tables();
        }
    }
    catch (...)
    {
        QSqlError err = q.lastError();
        QString e = err.text();
        qDebug() << e;
    }
}

void WorkoutDatabase::loadData()
{
    QFile file(QCoreApplication::applicationDirPath().append("/data/default_exercises.csv"));

    if ( !file.open(QFile::ReadOnly | QFile::Text) )
    {
        qDebug() << "File not exists";
    }
    else
    {
        // Create a thread to retrieve data from a file
        QTextStream in(&file);

        // Read the data up to the end of file
        while (!in.atEnd())
        {
            QString load_query = "INSERT INTO exercises (name, category) VALUES ";
            QString line = in.readLine();
            load_query.append("(" + line + ")");
            q.prepare(load_query);

            if (!q.exec())
            {
                QSqlError err = q.lastError();
                QString e = err.text();
                if (e != "UNIQUE constraint failed: exercises.name Unable to fetch row")
                {
                    qDebug() << e;
                    qDebug() << "failed to execute: " + load_query;
                }
            }
            q.clear();
        }

        file.close();

    }
}

std::vector<QString> WorkoutDatabase::getExercises(int numOfRows, std::vector<QString> exercises)
{
     QString data = "";
     q.clear();
     q.prepare("SELECT * FROM exercises");
     if (!q.exec())
     {
        qDebug() << "Failed to execute query: SELECT * FROM exercises";
        QSqlError err = q.lastError();
        QString e = err.text();
        qDebug() << e;
     }
     else
     {

        for (int i=0; i<numOfRows; i++)
        {
            q.next();
            data = q.value(1).toString();
            exercises.push_back(data);
        }
        return exercises;
      }
}

int WorkoutDatabase::get_number_of_exercises()
{
    q.clear();
    q.prepare("SELECT * FROM exercises");
    int numberOfRows = 0;
    if (!q.exec())
    {
       qDebug() << "Failed to execute query: SELECT * FROM exercises";
       QSqlError err = q.lastError();
       QString e = err.text();
       qDebug() << e;
    }
    else
    {
       if(q.last())
       {
           numberOfRows =  q.at() + 1;
           q.first();
           q.previous();
       }
    }
    return numberOfRows;

}

QStringList WorkoutDatabase::get_categories()
{
    QStringList * categories = new QStringList();
    q.clear();
    q.prepare("SELECT DISTINCT category FROM exercises");
    if (!q.exec())
    {
       qDebug() << "Failed to execute query: SELECT DISTINCT category FROM exercises";
       QSqlError err = q.lastError();
       QString e = err.text();
       qDebug() << e;
    }
    else
    {
        while (q.next())
        {
                *categories << q.value(1).toString();
        }
    }
    return *categories;
}
